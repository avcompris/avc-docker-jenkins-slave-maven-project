# avc-docker-jenkins-slave-maven-project

Docker image: avcompris/jenkins-slave-maven:project

Usage:

	$ docker run \
		-v /etc/ssl/private/jenkins:/etc/ssl/private/jenkins:ro \
		-e JENKINS_PUB_KEY="ssh-rsa AAAAB3Nz...NLsug/a7" \
		avcompris/jenkins-slave-maven:project

Exposed port is 22.
