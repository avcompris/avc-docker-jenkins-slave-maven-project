#/bin/bash

# File: avc-docker-jenkins-slave-maven-project/entrypoint.sh

set -e

if [ -z "${JENKINS_PUB_KEY}" ]; then
	echo "JENKINS_PUB_KEY should be set" >&2
	echo "Exiting." >&2
	exit 1
fi

echo "${JENKINS_PUB_KEY}" >> /home/jenkins/.ssh/authorized_keys

cp /etc/ssl/private/jenkins/jenkins.key /

chown jenkins: /jenkins.key

# start ssh
/usr/sbin/sshd -D
